﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoreLinq;
using ProductManager.Entities;
using System.Net.Mail;
using WCPG.DAL;
using Wordpress.Entities;
using WooCommerceNET.WooCommerce.v2;
using WooCommerceNET.WooCommerce.v2.Extension;
using WooCommerceNET;
using AutoMapper;
using WooProduct = WooCommerceNET.WooCommerce.v2.Product;

namespace NewWebsiteUpload
{
    class Program
    {
        private static IRepository<ProductManager.Entities.Product, ProductManagerDatabaseContext> productRepo { get; set; }
        private static IRepository<Prop65Chemical, ProductManagerDatabaseContext> chemRepo { get; set; }
        private static IRepository<WebProduct, WordpressDatabaseContext> webProductRepo { get; set; }
        private static IRepository<Field, WordpressDatabaseContext> fieldRepo { get; set; }
        private static IRepository<FacetGroup, WordpressDatabaseContext> categoryEntryRepo { get; set; }
        private static IMapper Mapper { get; set; }
        private static IImageService imageService { get; set; }

        private static WCObject wc { get; set; }

        static void Main(string[] args)
        {
            //NinjectContainer.Start();
            //var uow = NinjectContainer.Create<IUnitOfWork<ProductManagerDatabaseContext>>();
            //productRepo = NinjectContainer.Create<IRepository<ProductManager.Entities.Product, ProductManagerDatabaseContext>>();

            //RestAPI rest = new RestAPI("https://www.westchesterprotects.com/wp-json/wc/v2/", "ck_eac859b9b5659647651b20d5247024e18705e63d", "cs_4cea521b831bbf494b6b86a0e2653b30366f014d");
            //WCObject wc = new WCObject(rest);

            //var prd = productRepo.Get(x => x.BasePartNumber == "715SNFLW");
            //var wpprd = BuildWordpressProduct(prd);
            //var balhs = wc.Product.Add(wpprd).Result;


            Update();
        }

        static void DeleteAll()
        {
            RestAPI rest = new RestAPI("https://www.westchestergear.com/wp-json/wc/v2/", "ck_eac859b9b5659647651b20d5247024e18705e63d", "cs_4cea521b831bbf494b6b86a0e2653b30366f014d");
            WCObject wc = new WCObject(rest);

            bool endWhile = false;
            while (!endWhile)
            {
                var adfda = wc.Product.GetAll().Result;
                endWhile = adfda.Count == 0;

                foreach (var blah in adfda)
                {
                    var poo = wc.Product.Delete((int)blah.id).Result;
                }
            }
        }

        static List<WooCommerceNET.WooCommerce.v2.Product> GetAllProducts()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("per_page", "100");
            int pageNumber = 1;
            dic.Add("page", pageNumber.ToString());
            List<WooProduct> products = new List<WooProduct>();
            bool endWhile = false;
            while (!endWhile)
            {
                var productsTemp = wc.Product.GetAll(dic).Result;
                if (productsTemp.Count > 0)
                {
                    products.AddRange(productsTemp);
                    pageNumber++;
                    dic["page"] = pageNumber.ToString();
                }
                else
                {
                    endWhile = true;
                }
            }

            return products;
        }

        static void Update()
        {
            NinjectContainer.Start();
            var uow = NinjectContainer.Create<IUnitOfWork<ProductManagerDatabaseContext>>();
            productRepo = NinjectContainer.Create<IRepository<ProductManager.Entities.Product, ProductManagerDatabaseContext>>();
            chemRepo = NinjectContainer.Create<IRepository<ProductManager.Entities.Prop65Chemical, ProductManagerDatabaseContext>>();
            imageService = NinjectContainer.Create<IImageService>();

            RestAPI rest = new RestAPI("https://www.westchestergear.com/wp-json/wc/v2/", "ck_eac859b9b5659647651b20d5247024e18705e63d", "cs_4cea521b831bbf494b6b86a0e2653b30366f014d");
            wc = new WCObject(rest);

            var products = productRepo.GetAll().Where(x => !x.PrivateLabel && !x.TestProduct).OrderByDescending(x => x.BasePartNumber).ToList();//x.Facets.Any(f => f.FacetValue.Site == "Industrial") && !x.PrivateLabel && !x.TestProduct).OrderByDescending(x => x.BasePartNumber).ToList();
            var wpProducts = GetAllProducts();

            List<ProductManager.Entities.Product> failures = new List<ProductManager.Entities.Product>();
            string updatedProducts = "";
            for (int i = 0; i < products.Count; i++)
            {
                var product = products[i];
                try
                {
                    var wpProduct = wpProducts.FirstOrDefault(x => x.sku == product.BasePartNumber);

                    if (wpProduct != null)
                    {
                        //Only do the update if it has been modified.
                        if (product.PrimaryPhoto.Created >= DateTime.Now.Date.AddDays(-1) || product.dtModified >= DateTime.Now.Date.AddDays(-1) || (!product.ActiveIndustrial && !product.ActiveIroncat && !product.ActiveRetail) )
                        {
                            var updatedOrNewWpProduct = BuildWordpressProduct(product);
                            wpProduct.images.RemoveAll(x => true);
                            var doTheDamnThing = wc.Product.Update((int)wpProduct.id, wpProduct).Result;

                            var wooResult = wc.Product.Update((int)wpProduct.id, updatedOrNewWpProduct).Result;
                            updatedProducts += product.BasePartNumber + "<br />";
                        }
                    }
                    else
                    {
                        if (product.ActiveIndustrial || product.ActiveIroncat || product.ActiveRetail)
                        {
                            var updatedOrNewWpProduct = BuildWordpressProduct(product,true);
                            var wooResult = wc.Product.Add(updatedOrNewWpProduct).Result;
                            updatedProducts += product.BasePartNumber + "<br />";
                        }
                    }

                    product.OutdatedIndustrial = false;
                    productRepo.Update(product);
                }
                catch (Exception ex)
                {
                    string error = product.BasePartNumber + ":  " + ex.InnerException + ex.StackTrace;
                    using (System.IO.StreamWriter writ = new StreamWriter(@"\\wcmktg.com\WCPG-HQ\Public\edi\exe\WebsiteUpdater\errors\" + DateTime.Now.ToShortDateString().Replace("/","-") + ".txt", true))
                    {
                        writ.WriteLine(error);
                    }
                   failures.Add(product);
                }
                System.Diagnostics.Debug.WriteLine(i);
            }
            sendMail(updatedProducts);
            //Console.ReadLine();
        }

        static void sendMail(string message)
        {
            SmtpClient smtp = new SmtpClient("192.168.100.18", 25);
            string defaultRecipient = "JRettig@westchestergear.com";
            MailMessage email = new MailMessage();
            email.To.Add(defaultRecipient);
            email.To.Add("mmoore@westchestergear.com");
            email.From = new MailAddress("WebsiteUploader@westchestergear.com");
            email.Subject = "Products Updated Or Added";
            email.Body = message;
            email.IsBodyHtml = true;
            smtp.Send(email);
        }

        static WooCommerceNET.WooCommerce.v2.Product BuildWordpressProduct(ProductManager.Entities.Product product, bool newprod=false)
        {
            newprod = true;
            // Console.WriteLine(product.BasePartNumber + " " + product.Division + " " + product.ProductType + " " + product.ProductCategory);
            List<ProductImage> exportimages = new List<ProductImage>();
            int imgindex = 1;
            foreach (var photo in product.Photos.Where(x => x.Type == "Primary" || x.Type == "Website"))
            {
                string ext = Path.GetExtension(photo.Name);
                string fileName = product.BasePartNumber.Replace("/", "~") + ext;
                    //".png";
                Image image = Image.FromFile(@"\\fred\images\productimages\ImageUnavailable.png");
                if (photo.Created >= DateTime.Now.Date.AddDays(-1) || newprod)
                {
                    if (!File.Exists(@"\\fred\images\productimages\500\" + fileName))
                    {
                        if (File.Exists(@"\\fred\images\productimages\" + photo.Name))
                        {
                            try
                            {
                                image = Image.FromFile(@"\\fred\images\productimages\" + photo.Name);
                                image = imageService.ScaleImage(image, 500, 500);
                                image = imageService.PadWhite(image);
                                image.Save(@"\\fred\images\productimages\500\" + fileName);
                            }
                            catch
                            {
                                image = Image.FromFile(@"\\fred\images\productimages\ImageUnavailable.png");
                            }

                            image = Image.FromFile(@"\\fred\images\productimages\500\" + fileName);
                        }
                        else
                        {
                            image = Image.FromFile(@"\\fred\images\productimages\ImageUnavailable.png");
                        }
                    }
                    else
                    {
                        image = Image.FromFile(@"\\fred\images\productimages\500\" + fileName);
                    }

                    string newFile = @"\\fleabottom\HTML\wp-content\uploads\ProductImages\" + fileName;
                    if (File.Exists(newFile))
                        File.Delete(newFile);
                    image.Save(newFile);
                }
                ProductImage productimage = new ProductImage();
                productimage.alt = product.BasePartNumber;
                productimage.src = "https://www.westchestergear.com/wp-content/uploads/ProductImages/" + fileName;
                if (photo.Type == "Primary")
                {
                    productimage.position = 0;
                }
                else
                {
                    productimage.position = imgindex;
                    imgindex++;
                }
                exportimages.Add(productimage);
            }

            Dictionary<string, int> attrIDs = new Dictionary<string, int>();
            attrIDs.Add("Size", 1);
            attrIDs.Add("Item Type", 2);
            attrIDs.Add("Item Style", 3);
            attrIDs.Add("Protection Need", 4);
            attrIDs.Add("Material", 5);
            attrIDs.Add("Quality", 6);
            attrIDs.Add("Brand", 7);
            attrIDs.Add("Industry", 8);
            attrIDs.Add("ANSI Performance Class", 11);
            attrIDs.Add("ANSI Garment Type", 12);
            attrIDs.Add("Hi-Viz Category", 13);
            attrIDs.Add("Pattern", 14);
            attrIDs.Add("Color", 15);
            attrIDs.Add("Closure", 16);
            attrIDs.Add("Reflective Tape", 17);
            attrIDs.Add("Storage", 18);
            attrIDs.Add("Prop 65", 21);
            attrIDs.Add("Amazon ID", 22);
            attrIDs.Add("Application", 23);
            attrIDs.Add("Product Type", 24);
            attrIDs.Add("Discontinued", 25);

            string specName = product.BasePartNumber.Replace("/", "~") + ".pdf";

            var specfile = @"\\fleabottom\HTML\wp-content\uploads\ProductSpecSheets\" + specName;
            if (File.Exists(specfile))
                File.Delete(specfile);
            if (File.Exists(@"\\fred\PMFiles\CustomerSpecs\" + specName))
                File.Copy(@"\\fred\PMFiles\CustomerSpecs\" + specName, specfile);

            //Add new product

            ProductAttributeLine attr;
            var attributes = new List<ProductAttributeLine>();
            var facetGroups = product.Facets
                .Where(x => x.FacetValue.Site == "Industrial")
                .OrderBy(x => x.FacetValue.Category)
                .GroupBy(x => x.FacetValue.Category).Select(x => x.ToList()).ToList();

            int pos = 0;
            for (int i = 0; i < facetGroups.Count; i++)
            {
                pos = i;
                var group = facetGroups[i];

                attr = new ProductAttributeLine();
                attr.name = group[0].FacetValue.Category;
                attr.position = pos;
                attr.options = new List<string>();
                attr.id = attrIDs[attr.name];

                attr.visible = attr.id == 1 || attr.id == 1;

                foreach (var facet in group)
                {
                    attr.options.Add(facet.FacetValue.Value.Equals(null)?"":facet.FacetValue.Value.ToString());
                }
                if (attr.options.Count < 1)
                {
                    attr.options.Add("");
                }
                attributes.Add(attr);
            }

            pos++;
            attr = new ProductAttributeLine();
            attr.name = "Marketing Copy";
            attr.position = pos;
            attr.options = new List<string>()
                {
                    product.MarketingCopy
                };
            attr.id = 9;
            attr.visible = false;
            attributes.Add(attr);

            attr = new ProductAttributeLine();
            attr.name = "Brand";
            attr.position = pos;
            attr.options = new List<string>()
                {
                    product.Brand
                };
            attr.id = 7;
            attr.visible = false;
            attributes.Add(attr);

            pos++;
            attr = new ProductAttributeLine();
            attr.name = "Features";
            attr.position = pos;
            attr.options = new List<string>();
            attr.id = 10;
            attr.visible = true;

            foreach (var feature in product.Features.OrderBy(x => x.Rank))
            {
                attr.options.Add(feature.Value);
            }
            if (attr.options.Count < 1)
            {
                attr.options.Add("");
            }
            attributes.Add(attr);

            pos++;
            attr = new ProductAttributeLine();
            attr.name = "Real Name";
            attr.position = pos;
            attr.options = new List<string>();
            attr.options.Add(product.Name);
            attr.id = 19;
            attr.visible = false;
            attributes.Add(attr);

            pos++;
            attr = new ProductAttributeLine();
            attr.name = "Real SKU";
            attr.position = pos;
            attr.options = new List<string>();
            attr.id = 20;
            attr.visible = true;

            foreach (var sku in product.Items.Where(x => x.Active).SelectMany(x => x.PackSizes).Select(x => x.SKU))
            {
                attr.options.Add(sku);
            }
            if (attr.options.Count < 1)
            {
                attr.options.Add("");
            }
            attributes.Add(attr);
            if (!string.IsNullOrWhiteSpace(product.Prop65Chemicals))
            {
                pos++;
                attr = new ProductAttributeLine();
                attr.name = "Prop 65";
                attr.position = pos;
                attr.options = new List<string>();
                attr.id = 21;
                attr.visible = true;

                foreach (var p65Chem in product.Prop65Chemicals.Split(','))
                {
                    //var realChem = chemRepo.Get(x => x.Name == p65Chem);
                    //attr.options.Add(realChem.Warning);
                    attr.options.Add(p65Chem);
                }
                if (attr.options.Count < 1)
                {
                    attr.options.Add("");
                }
                attributes.Add(attr);
            }
 pos++;
            /*       attr = new ProductAttributeLine();
                   attr.name = "Amazon ID";
                   attr.position = pos;
                   attr.options = new List<string>();
                   attr.id = 22;
                   attr.visible = true;
                   attr.options.Add(product.AmazonLink.Equals(null) ? "":product.AmazonLink.ToString());
                   attributes.Add(attr);*/

            /* pos++;
             attr = new ProductAttributeLine();
             attr.name = "product-type";
             attr.position = pos;
             attr.options = new List<string>()
                 {
                     product.ProductCategory
                 };
             attr.id = 24;
             attr.visible = false;
             attributes.Add(attr);
             */

            attr = new ProductAttributeLine();
            attr.name = "Discontinued";
            attr.position = pos;
            attr.options = new List<string>();
            if (product.Discontinued)
            {
                attr.options.Add("While Supplies Last");
            } else
            {
                attr.options.Add("");
            }
            attr.id = 25;
            attr.visible = false;
            attributes.Add(attr);


            var cats = new List<ProductCategoryLine>();

            if (product.Division == "Retail" || product.Division=="Both")
            {
                cats.Add(new ProductCategoryLine()
                {
                    id = 8611,
                    name = "Retail"
                });
                if (product.ProductType == "Accessories")
                {
                    cats.Add(new ProductCategoryLine()
                    {
                        id = 8960,
                        name = "Accessories"
                    });
                    if (product.ProductCategory == "Umbrella")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8973,
                            name = "Umbrella"
                        });
                    }
                    else if (product.ProductCategory == "Garden Accessories")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8974,
                            name = "Garden Accessories"
                        });
                    }
                    else if (product.ProductCategory == "Winter Accessories")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8975,
                            name = "Winter Accessories"
                        });
                    }
                }
                else if (product.ProductType == "Gloves")
                {
                    cats.Add(new ProductCategoryLine()
                    {
                        id = 8958,
                        name = "Gloves"
                    });
                    if (product.ProductCategory == "General Purpose")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8962,
                            name = "General Purpose"
                        });
                    }
                    else if (product.ProductCategory == "Leather")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8963,
                            name = "Leather"
                        });
                    }
                    else if (product.ProductCategory == "Coated")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8964,
                            name = "Coated"
                        });
                    }
                    else if (product.ProductCategory == "Performance/ Hi Dex")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8965,
                            name = "Performance/Hi-Dex"
                        });
                    }
                    else if (product.ProductCategory == "Chemical")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8966,
                            name = "Chemical"
                        });
                    }
                    else if (product.ProductCategory == "Lawn & Garden")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8967,
                            name = "Lawn & Garden"
                        });
                    }
                    else if (product.ProductCategory == "Winter")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8968,
                            name = "Winter"
                        });
                    }
                    else if (product.ProductCategory == "Disposable")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8969,
                            name = "Disposable"
                        });
                    }
                }
                else if (product.ProductType == "Apparel")
                {
                    cats.Add(new ProductCategoryLine()
                    {
                        id = 8959,
                        name = "Apparel"
                    });
                    if (product.ProductCategory == "Rainwear")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8970,
                            name = "Rainwear"
                        });
                    }
                    else if (product.ProductCategory == "Safety Apparel")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8971,
                            name = "Safety Apparel"
                        });
                    }
                    else if (product.ProductCategory == "Disposable Clothing")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8972,
                            name = "Disposable Clothing"
                        });
                    }
                }
                else if (product.ProductType == "PPE")
                {
                    cats.Add(new ProductCategoryLine()
                    {
                        id = 8961,
                        name = "PPE"
                    });
                    if (product.ProductCategory == "Hard Hat")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8976,
                            name = "Hard Hat"
                        });
                    }
                    else if (product.ProductCategory == "Safety Equipment")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8977,
                            name = "Safety Equipment"
                        });
                    }
                    else if (product.ProductCategory == "Respiration")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8978,
                            name = "Respiration"
                        });
                    }
                    else if (product.ProductCategory == "Ear Protection")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8979,
                            name = "Ear Protection"
                        });
                    }
                    else if (product.ProductCategory == "Eye Protection")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8980,
                            name = "Eye Protection"
                        });
                    }
                }
            }
            if (product.Division == "Industrial" || product.Division=="Both")
            {
                cats.Add(new ProductCategoryLine()
                {
                    id = 8612,
                    name = "Industrial"
                });
                if (product.ProductType == "Apparel")
                {
                    cats.Add(new ProductCategoryLine()
                    {
                        id = 8982,
                        name = "Apparel"
                    });
                    if (product.ProductCategory == "Boufant/ booties/ etc.")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8992,
                            name = "Boufant/Booties/etc."
                        });
                    }
                    else if (product.ProductCategory == "Disposable Clothing")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8993,
                            name = "Disposable Clothing"
                        });
                    }
                    else if (product.ProductCategory == "Rainwear")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8994,
                            name = "Rainwear"
                        });
                    }
                    else if (product.ProductCategory == "Safety Apparel")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8995,
                            name = "Safety Apparel"
                        });
                    }
                    else if (product.ProductCategory == "Sleeves")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8996,
                            name = "Sleeves"
                        });
                    }
                }
                else if (product.ProductType == "Gloves")
                {
                    cats.Add(new ProductCategoryLine()
                    {
                        id = 8981,
                        name = "Gloves"
                    });
                    if (product.ProductCategory == "Coated")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8984,
                            name = "Coated"
                        });
                    }
                    else if (product.ProductCategory == "Cut Resistant")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8985,
                            name = "Cut Resistant"
                        });
                    }
                    else if (product.ProductCategory == "Leather")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8986,
                            name = "Leather"
                        });
                    }
                    else if (product.ProductCategory == "Performance/ Hi Dex")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8987,
                            name = "Performance/Hi-Dex"
                        });
                    }
                    else if (product.ProductCategory == "General Purpose")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8988,
                            name = "General Purpose"
                        });
                    }
                    else if (product.ProductCategory == "Chemical Resistant")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8989,
                            name = "Chemical Resistant"
                        });
                    }
                    else if (product.ProductCategory == "Disposable")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8990,
                            name = "Disposable"
                        });
                    }
                    else if (product.ProductCategory == "Winter")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8991,
                            name = "Winter"
                        });
                    }
                }
                else if (product.ProductType == "PPE")
                {
                    cats.Add(new ProductCategoryLine()
                    {
                        id = 8983,
                        name = "PPE"
                    });
                    if (product.ProductCategory == "Hard Hat")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8997,
                            name = "Hard Hat"
                        });
                    }
                    else if (product.ProductCategory == "Safety Equipment")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8998,
                            name = "Safety Equipment"
                        });
                    }
                    else if (product.ProductCategory == "Respiration")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 8999,
                            name = "Respiration"
                        });
                    }
                    else if (product.ProductCategory == "Ear Protection")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 9000,
                            name = "Ear Protection"
                        });
                    }
                    else if (product.ProductCategory == "Eye Protection")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 9001,
                            name = "Eye Protection"
                        });
                    }
                }
            }
            if (product.Division == "Welding")
            {
              //  Console.WriteLine("Welding");
                cats.Add(new ProductCategoryLine(){ id = 8613 });
                if (product.ProductType == "Gloves")
                {
                    cats.Add(new ProductCategoryLine()
                    {
                        id = 9002
                    });
                    //Console.WriteLine("Gloves");
                    if (product.ProductCategory == "Stick")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 9004
                        });
                    }
                    else if (product.ProductCategory == "Tig")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 9006
                        });
                    }
                    else if (product.ProductCategory == "Mig")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 9005
                        });
                       // Console.WriteLine("Mig");
                    }
                    else if (product.ProductCategory == "General Purpose")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 9007
                        });
                    }
                }
                else if (product.ProductType == "Apparel")
                {
                    cats.Add(new ProductCategoryLine()
                    {
                        id = 9003
                    });
                    if (product.ProductCategory == "Boot Covers")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 9008
                        });
                    }
                    if (product.ProductCategory == "Greens/ Apparel")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 9009
                        });
                    }
                    if (product.ProductCategory == "Leather")
                    {
                        cats.Add(new ProductCategoryLine()
                        {
                            id = 9010
                        });
                    }
                }
            }


                cats.Add(new ProductCategoryLine()
                {
                    id = 2993,
                    name = "All Products"
                });


            if (product.SpecSheetCategory == "Hi-Vis" || product.SAPGroupNumber=="HV" || product.SAPGroupNumber=="HVRW")
            {
                cats.Add(new ProductCategoryLine()
                {
                    id = 138,
                    name = "Hi-Viz"
                });
            }
            
            WooCommerceNET.WooCommerce.v2.Product p = new WooCommerceNET.WooCommerce.v2.Product()
                {
                    name = product.BasePartNumber,
                    description = product.Description,
                    sku = product.BasePartNumber,
                    images = exportimages,
                    attributes = attributes,
                    categories = cats,
                    status = (product.ActiveIndustrial || product.ActiveIroncat || product.ActiveRetail) ? "publish" : "private"
                };
            //Console.WriteLine(product.BasePartNumber + " - Completed");
            return p;
        }
    }
}
