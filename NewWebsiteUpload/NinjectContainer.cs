﻿using System;
using AutoMapper;
using Ninject;
using Ninject.Extensions.Conventions;

namespace WCPG.DAL
{
    public static class NinjectContainer
    {
        private static IKernel _ninjectKernel;

        public static void Start()
        {
            _ninjectKernel = new StandardKernel();
            ConfigureDependency();
        }

        public static T Create<T>()
        {
            return _ninjectKernel.Get<T>();
        }

        private static void ConfigureDependency()
        {
            _ninjectKernel.Bind<IUnitOfWork<ProductManagerDatabaseContext>>().To<UnitOfWork<ProductManagerDatabaseContext>>().InThreadScope();
            _ninjectKernel.Bind(x => x.FromAssembliesMatching("*").SelectAllClasses().BindDefaultInterface());
            var config = new MapperConfiguration(cfg =>
            {

            });
            _ninjectKernel.Bind<IMapper>().ToConstant(config.CreateMapper());
        }
    }
}