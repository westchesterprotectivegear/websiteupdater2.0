﻿using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Context;

namespace WCPG.DAL
{
    public interface DatabaseContext
    {
        ISessionFactory GetSessionFactory();
    }

    public class ProductManagerDatabaseContext : DatabaseContext
    {
        public ISessionFactory GetSessionFactory()
        {
            return Fluently.Configure()
                    .Database(MsSqlConfiguration.MsSql2008.ConnectionString(c => c.FromConnectionStringWithKey("ProductManagerConnection")))
                    .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("ProductManager.Entities")))
                    .CurrentSessionContext<WebSessionContext>()
                    .BuildSessionFactory();
        }
    }

    public class WordpressDatabaseContext : DatabaseContext
    {
        public ISessionFactory GetSessionFactory()
        {
            return Fluently.Configure()
                          .Database(MySQLConfiguration.Standard.ConnectionString(c => c.FromConnectionStringWithKey("WordpressConnection")))
                          .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("Wordpress.Entities")))
                          .CurrentSessionContext<WebSessionContext>()
                          .BuildSessionFactory();
        }
    }
}