﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace Wordpress.Entities
{
    public class WebProduct
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
        public virtual string Slug { get; set; }
        public virtual string Description { get; set; }
        public virtual string PhotoURL { get; set; }
        public virtual int CategoryID { get; set; }
        public virtual string DateCreated { get; set; }
        public virtual IList<Field> Fields { get; set; }
        public virtual IList<FacetGroup> CatalogEntries { get; set; }
    }

    public class WebProductMapping : ClassMap<WebProduct>
    {
        public WebProductMapping()
        {
            Table("wp_UPCP_Items");
            Id(x => x.ID, "Item_ID");
            Map(x => x.Name, "Item_Name");
            Map(x => x.Slug, "Item_Slug");
            Map(x => x.Description, "Item_Description");
            Map(x => x.PhotoURL, "Item_Photo_URL");
            Map(x => x.CategoryID, "Category_ID");
            Map(x => x.DateCreated, "Item_Date_Created");
            HasMany(x => x.Fields)
                .KeyColumn("Item_ID");
            HasMany(x => x.CatalogEntries)
                .KeyColumn("Item_ID");
        }
    }
}
