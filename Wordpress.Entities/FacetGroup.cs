﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace Wordpress.Entities
{
    public class FacetGroup
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
        public virtual string Label { get; set; }
    }

    public class CatalogEntryMapping : ClassMap<FacetGroup>
    {
        public CatalogEntryMapping()
        {
            Table("wp_woocommerce_attribute_taxonomies");
            Id(x => x.ID, "attribute_id");
            Map(x => x.Name, "attribute_name");
            Map(x => x.Label, "attribute_label");
        }
    }
}
