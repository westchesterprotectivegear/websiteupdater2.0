﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace Wordpress.Entities
{
    public class Field
    {
        public virtual int ID { get; set; }
        public virtual int FieldID { get; set; }
        public virtual int ItemID { get; set; }
        public virtual string Value { get; set; }
    }

    public class FieldMapping : ClassMap<Field>
    {
        public FieldMapping()
        {
            Table("wp_UPCP_Fields_Meta");
            Id(x => x.ID, "Meta_ID");
            Map(x => x.FieldID, "Field_ID");
            Map(x => x.ItemID, "Item_ID");
            Map(x => x.Value, "Meta_Value");
        }
    }
}
